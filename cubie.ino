/*
Cubie
 by codea.me
 
A desktop colored lamp.
 */

#include <EEPROM.h>
#include <MeetAndroid.h>


#define CONFIG_VERSION "E78061"
#define CONFIG_START 0


MeetAndroid meetAndroid;
//Light Sensor (ldr)
const int ldrPin = A3;
//PWM CONTROL FOR RGB LEDS.
const int redLed = 9;
const int greenLed = 10;
const int blueLed = 11;

//Control Variables
int lightON;
float tcount;
int rb_color;

//config comming from Bluetooth
int redValue;
int greenValue;
int blueValue;
int mode;
int useLightSensor;


//SETUP
void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  /* Notes on my HC-06 Bluetooth device:
     I am using an arduino Mega 2560 R3, it has 3 hardware serial ports.
     Unfortunately my chinese ainol tablet does not have Bluetooth and is not compatible [firmware i think] with external usb-bluetooth dongles.
     Can cuse 3.3v or 5v.
     Must use a level shifter for the HC-06 TX pin to the arduino RX pin. I used a 7404 (NOT or inverter gate) as a level shifter [why buy one when i have an inverter that can work as a level-shifter?]
     If not used a level-shifter, the arduino always recevie a '0' when receiving data [actually a logic zero because the voltage is too low for the arduino to interpret as a one].
     
     Notes on Amarino:
     
     Original Amarino (MeetAndroid library for arduino) uses the Serial (hardware) port. It has one problem when doing tests: it is used to program the arduino via USB and debugging using the Serial port.
     The MeetAndroid uses the Serial to which the BT is connected. This is hardcoded in the MeetAndroid. Most arduinos include only one serial port (hardware), but Mega has 4 (Serial, Serial1, Serial2, Serial3).
     As i am using a Mega to develop cubie, i can use the Serial2 for the BT. But using other board would bring the problem with hardware serial port (have to disconect BT each time i want to program the arduino).
     Im planing to use an arduino Pro mini for cubie, to help reduce hardware size and accomodate it on a small cube lamp. The pro mini has only one Serial port. Problems when updating firmware on production?
  */
  
  pinMode(ldrPin, INPUT);
 
  pinMode(redLed, OUTPUT); 
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  
  //Amarino callbacks...
  meetAndroid.registerFunction(setRed, 'r');
  meetAndroid.registerFunction(setGreen, 'g');  
  meetAndroid.registerFunction(setBlue, 'b'); 
  meetAndroid.registerFunction(setMode, 'm');
  meetAndroid.registerFunction(setAutoOff, 'l'); // activa el sensor de luz ambiental
  
  lightON = 0; //turn off light when starting up.
  tcount = 0.0;
  rb_color = 0;
  
  //get saved values from memory.
  loadConfig();
}


//Amarino Callbacks

void setMode(byte flag, byte numOfMode) 
{
  mode = meetAndroid.getInt();
  Serial.print("Mode changed to "); Serial.println(mode);
  saveConfig();
}


//TODO: Add a config option to allow to setup [calibrate] the sensor level for triggering the on/off state. 
void setAutoOff(byte flag, byte val) 
{
  useLightSensor = meetAndroid.getInt();
  Serial.print("Auto OFF:"); Serial.println(useLightSensor);
  saveConfig();
}

void setRed(byte flag, byte numOfValues)
{
  if (mode == 3) {
    //if we are in mode 3 - Arcoiris, we discard the arraived color....
    return;
  }
  
  int c = meetAndroid.getInt();
  redValue = c;
  analogWrite(redLed, c); //Call rgb() Instead of analogWrite
  //Serial.print("RED "); Serial.println(c);
  saveConfig();
}

void setGreen(byte flag, byte numOfValues)
{
  if (mode == 3) {
    //if we are in mode 3 - Arcoiris, we discard the arraived color....
    return;
  }
  
  int c = meetAndroid.getInt();
  greenValue = c;
  analogWrite(greenLed, c);  //Call rgb() Instead of analogWrite
  //Serial.print("GREEN "); Serial.println(c);
  saveConfig();
}

void setBlue(byte flag, byte numOfValues)
{
  if (mode == 3) {
    //if we are in mode 3 - Arcoiris, we discard the arraived color....
    return;
  }
  
  int c = meetAndroid.getInt();
  blueValue = c;
  analogWrite(blueLed, c); //Call rgb() Instead of analogWrite
  //Serial.print("BLUE "); Serial.println(c);
  saveConfig();
}


/* CONFIG STUFF */

void loadConfig() {
  //check if we have written the settings on eeprom.
  if (EEPROM.read(CONFIG_START + 0) == CONFIG_VERSION[0] && //read the first byte of the config_version
      EEPROM.read(CONFIG_START + 1) == CONFIG_VERSION[1] && //read the second byte of the config_version... an so on.
      EEPROM.read(CONFIG_START + 2) == CONFIG_VERSION[2] &&
      EEPROM.read(CONFIG_START + 3) == CONFIG_VERSION[3] &&
      EEPROM.read(CONFIG_START + 4) == CONFIG_VERSION[4] &&
      EEPROM.read(CONFIG_START + 5) == CONFIG_VERSION[5]) {
        //ok the config is written by us.
        //the config_version is from byte 0 to byte 5
        mode = EEPROM.read(CONFIG_START + 6); // MODE is in 7th byte (starting from 0, its 6th)
        useLightSensor = EEPROM.read(CONFIG_START + 7);
        redValue = EEPROM.read(CONFIG_START + 8);
        greenValue = EEPROM.read(CONFIG_START + 9);
        blueValue = EEPROM.read(CONFIG_START + 10);
        Serial.println("Configuration Check VALID, getting values from memory");
        Serial.print("MODE: "); Serial.print(mode); Serial.print("\tUseLightSensor:");Serial.print(useLightSensor); Serial.print("\tRED:"); Serial.print(redValue); Serial.print("\tGreen:"); Serial.print(greenValue); Serial.print("\tBlue:"); Serial.println(blueValue);
      } else {
        //The config is not written by us on the current version. So, load default values
        mode = 1;
        useLightSensor = 0;
        redValue = 85;
        greenValue = 13;
        blueValue = 0;
        saveConfig(); // as config was invalid, save default values.
        Serial.println("Configuration Check INVALID, saving default values");
      }
}

void saveConfig() {
  // Only write config to eeprom if current value is different on eeprom. Since write cycles are limited to 100,000, we need to be careful about writing.
  
  if (EEPROM.read(CONFIG_START + 0) == CONFIG_VERSION[0] && //read the first byte of the config_version
      EEPROM.read(CONFIG_START + 1) == CONFIG_VERSION[1] && //read the second byte of the config_version... an so on.
      EEPROM.read(CONFIG_START + 2) == CONFIG_VERSION[2] &&
      EEPROM.read(CONFIG_START + 3) == CONFIG_VERSION[3] &&
      EEPROM.read(CONFIG_START + 4) == CONFIG_VERSION[4] &&
      EEPROM.read(CONFIG_START + 5) == CONFIG_VERSION[5]) {} else {
        EEPROM.write(CONFIG_START +0, CONFIG_VERSION[0]);
        EEPROM.write(CONFIG_START +1, CONFIG_VERSION[1]);
        EEPROM.write(CONFIG_START +2, CONFIG_VERSION[2]);
        EEPROM.write(CONFIG_START +3, CONFIG_VERSION[3]);
        EEPROM.write(CONFIG_START +4, CONFIG_VERSION[4]);
        EEPROM.write(CONFIG_START +5, CONFIG_VERSION[5]);
        Serial.println("Config Check was not VALID, Writting new config check");
      }
  
  if (EEPROM.read(CONFIG_START + 6) != mode) {
      EEPROM.write(CONFIG_START + 6, mode);
      Serial.println("Writting mode");
      char chars[10];
      String str;
      str = "M";
      str += mode;
      str.toCharArray(chars,10);
      Serial.print("Sending to phone:"); Serial.println(chars);
      //Send value to save to the android app.
      meetAndroid.send(chars);
  }
  if (EEPROM.read(CONFIG_START + 7) != useLightSensor) {
      EEPROM.write(CONFIG_START + 7, useLightSensor);
      Serial.println("Writting UseLightSensor");
      char chars[10];
      String str;
      str = "L";
      str += useLightSensor;
      str.toCharArray(chars,10);
      Serial.print("Sending to phone:"); Serial.println(chars);
      meetAndroid.send(chars);
  }
  if (EEPROM.read(CONFIG_START + 8) != redValue) {
      EEPROM.write(CONFIG_START + 8, redValue);
      Serial.println("Writting RED Value");
      char chars[10];
      String str;
      str = "R";
      str += redValue;
      str.toCharArray(chars,10);
      Serial.print("Sending to phone:"); Serial.println(chars);
      meetAndroid.send(chars);
  }
  if (EEPROM.read(CONFIG_START + 9) != greenValue) {
      EEPROM.write(CONFIG_START + 9, greenValue);
      Serial.println("Writting GREEN Value");
      char chars[10];
      String str;
      str = "G";
      str += greenValue;
      str.toCharArray(chars,10);
      Serial.print("Sending to phone:"); Serial.println(chars);
      meetAndroid.send(chars);
  }
  if (EEPROM.read(CONFIG_START + 10) != blueValue) {
       EEPROM.write(CONFIG_START + 10, blueValue);
       Serial.println("Writting BLUE Value");
       char chars[10];
       String str;
       str = "B";
       str += blueValue;
       str.toCharArray(chars,10);
       Serial.print("Sending to phone:"); Serial.println(chars);
       meetAndroid.send(chars);
  }
  
  //Now we send the written values to android app for storing on the phone.
  //We do this because if we save on android without the values returned here from arduino, then the android can be out of sync due to bluetooth conection failure
  //So we ensure that we save values on android wich are stored on the eeprom in the arduino.
  //Yes, some problem with bluetooth can make not syncing...
  
  //Serial.print("MODE: "); Serial.print(mode); Serial.print("\tUseLightSensor:");Serial.print(useLightSensor); Serial.print("\tRED:"); Serial.print(redValue); Serial.print("\tGreen:"); Serial.print(greenValue); Serial.print("\tBlue:"); Serial.println(blueValue);  
}


/* SECCION DE EFECTOS */

//Set Color
void rgb(int r, int g, int b) {
  analogWrite(redLed, r);
  analogWrite(greenLed, g);
  analogWrite(blueLed, b);
}


// Efecto de vela o candil, parpadeante.
void candil() {
  int vr, vg,vb;
  //vr = random(80, 90); //200,220
  //vg = random(10,20); //10,20
  //vb = random(0,3); //0,10
  vr = random(redValue-5, redValue+5);
  vg = random(greenValue-5, greenValue+5);
  vb = random( blueValue-5, blueValue+5);
  if (vr < 0) vr =0; if (vr > 255) vr = 255;
  if (vg < 0) vg =0; if (vg > 255) vg = 255;
  if (vb < 0) vb =0; if (vb > 255) vb = 255;
  rgb(vr,vg,vb);
  //Serial.print("R:"); Serial.print(vr); Serial.print("\tG:"); Serial.print(vg); Serial.print("\tB:"); Serial.println(vb);
  delay(60+random(40));
} 

void rainbow() {
    tcount = tcount + 0.1; //this var is shared with the Pulse effect
    if (tcount > 3.14) {
      tcount = 0.0;
      rb_color++; //each sine cycle we change the color! :D
    }
    float bright = sin(tcount);
    if (rb_color > 6) rb_color=0;
    switch ( rb_color ) {
      case 0:  //RED
             rgb(255*bright, 0, 0);
             break;
      case 1:  // ORANGE
             rgb(255*bright,10*bright,0);
             break;
      case 2: //YELLOW
             rgb(240*bright,30*bright,0);
             break;
      case 3: //GREEN
            rgb(0,255*bright,0);
            break;
      case 4: //BLUE
            rgb(0,0,255*bright);
            break;
      case 5: //INDIGO
            rgb(128*bright,0,255*bright);
            break;
      case 6: //VIOLET
            rgb(255*bright,0,128*bright);
            break;
    }
    delay(50);
    //Serial.print("COLOR:"); Serial.print(rb_color); Serial.print(" BRIGHT: "); Serial.print(bright); Serial.print(" tcount:"); Serial.println(tcount);
}

void pulse() {
  tcount = tcount + 0.1;
  if (tcount > 3.14) tcount = 0.0;
  float bright = sin(tcount);
  rgb(int(redValue*bright), int(greenValue*bright), int(blueValue*bright)); 
  //Serial.print("tcount:"); Serial.print(tcount); Serial.print("Bright:"); Serial.println(bright);
  delay(100);
}




/* SECCION DE CONTROL */

void updateLight() {
  if (lightON == 1) { 
    // LIGTH IS ACTIVE, check current effect to apply
    switch(mode)
    {
      case 1: candil(); break;                                // CANDLE EFFECT
      case 2: rgb(redValue, greenValue, blueValue); break;   // SOLID COLOR
      case 3: rainbow(); break;
      case 4: pulse(); break;
      default: candil(); break;   // DEFAULT IS CANDLE EFFECT.
    } //switch mode
  } else {
    //turn off
    rgb(0,0,0);
  }
}



/*  MAIN LOOP: Check inputs and apply settings according to them */

void loop() {
  meetAndroid.receive();
  // read the LDR at pin A7.
  delay(10);
  int ldrValue = analogRead(ldrPin);
  //Serial.print("Valor del LDR:"); Serial.println(ldrValue);
  if (useLightSensor == 1) {
    if ( ldrValue > 850) {
      // AMBIENT LIGHT FROM SENSOR IS LOW
      if (lightON == 0) {
        lightON = 1;
      } else {   }
    }// if ldr >700
    else {
      // AMBIENT LIGHT FROM SENSOR IS HIGH
      if (lightON == 1 ) {
        lightON = 0;
      } else {   }
    }
  } //uselightSensor
  else { lightON = 1; }
  
  updateLight();

  //TESING BLUETOOTH
  //if (Serial1.available() > 0) {
    //Serial.print(Serial1.available()); Serial.print(" bytes available at BT serial line: ");
    //int inByte = Serial1.read();
    //Serial.print("0x"); Serial.print(inByte, HEX); Serial.print("h\t"); Serial.println(inByte, DEC);
  //}
  
  // for debugging and testing commands from serial to Serial1
  if (Serial.available()) {
    int inByte = Serial.read();
    Serial1.write(inByte); 
  }
}
